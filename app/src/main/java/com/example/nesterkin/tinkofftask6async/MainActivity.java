package com.example.nesterkin.tinkofftask6async;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Написать класс MyObservable:
 *
 * MyObservable.from(callable)
 * .subscribeOn(looper)
 * .observeOn(mainLooper)
 * .subscribe(callback);
 *
 * Выполнение стартует только после subscribe()
 * subscribeOn — на каком лупере будет выполняться callable; если не указан, выполнится на том треде, в котором вызвали subscribe
 * observeOn — на каком лупере будет выполняться callback; если не указан выполнится на треде subscribeOn
 * Порядок subscribeOn и observeOn не важен
 * callable и callback — собственные классы
 * Просьба логгировать текущий тред выполнения
 */
public class MainActivity extends AppCompatActivity {

    private TextView mTextView;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = findViewById(R.id.text_view);

        HandlerThread handlerThread = new HandlerThread("backgroundThread");
        handlerThread.start();

        MyObservable myObservable = new MyObservable();
        myObservable.from(new MyCallable())
                .subscribeOn(handlerThread.getLooper())
                .observeOn(getMainLooper())
                .subscribe(myEntity -> mTextView.setText(myEntity.getString() + ", " + myEntity.getInt() + " godikov"));
    }
}