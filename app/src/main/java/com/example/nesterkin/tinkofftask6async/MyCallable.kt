package com.example.nesterkin.tinkofftask6async

import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

/**
 * @author Nesterkin Alexander on 06/11/2018
 */
class MyCallable : Callable<MyEntity> {

    override fun call(): MyEntity {
        try {
            TimeUnit.SECONDS.sleep(2)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        return MyEntity("Sashulya", 26)
    }
}