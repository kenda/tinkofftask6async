package com.example.nesterkin.tinkofftask6async

/**
 * @author Nesterkin Alexander on 06/11/2018
 */
data class MyEntity(val string: String,
                    val int: Int)