package com.example.nesterkin.tinkofftask6async

/**
 * @author Nesterkin Alexander on 06/11/2018
 */
interface MyCallback {
    fun callback(myEntity: MyEntity)
}