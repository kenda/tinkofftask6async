package com.example.nesterkin.tinkofftask6async;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.Callable;

/**
 * @author Nesterkin Alexander on 05/11/2018
 */
public class MyObservable {

    private static final String LOG_TAG = "MyObservable";

    private static final int MSG_CODE = 1234;

    private Callable mCallable;
    private Looper mSubscribeOnLooper;
    private Looper mObserveOnLooper;

    public MyObservable from(Callable callable) {
        mCallable = callable;
        return this;
    }

    MyObservable subscribeOn(Looper looper) {
        mSubscribeOnLooper = looper;
        return this;
    }

    MyObservable observeOn(Looper looper) {
        mObserveOnLooper = looper;
        return this;
    }

    void subscribe(MyCallback myCallback) {
        Handler subsHandler = new Handler(mSubscribeOnLooper);

        Handler obsHandler = new Handler(mObserveOnLooper) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MSG_CODE) {
                    Log.v(LOG_TAG, Thread.currentThread().toString());
                    myCallback.callback((MyEntity) msg.obj);
                }
            }
        };

        subsHandler.post(() -> {
            try {
                Message message = new Message();
                message.obj = mCallable.call();
                message.what = MSG_CODE;

                obsHandler.sendMessage(message);
                Log.v(LOG_TAG, Thread.currentThread().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Message message = obsHandler.obtainMessage();
        obsHandler.handleMessage(message);
    }
}